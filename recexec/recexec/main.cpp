#include <QDirIterator>
#include <QProcess>
#include <QFileInfo>

#include <iostream>
#include <vector>
#include <string>
#include <sys/stat.h>


#define VERSION 1.2

bool can_exec(const std::string& file)
{
    struct stat  st;

    if (stat(file.c_str(), &st) < 0)
        return false;
    if ((st.st_mode & S_IEXEC) != 0)
        return true;
    return false;
}

void print(const std::string& str)
{
    std::cout << str << std::endl;
}

void print(const QString& str)
{
    print(str.toStdString());
}

void print(const char* str)
{
    print(std::string(str));
}

int print_usage()
{
    print("recexec [start location] [options]");
    print("options:");
    print("-i pattern\t\tinclude pattern (eg. *Test*.exe)");
    print("-c command args...\tcommand template");
    print("-v\t\t\tverbose mode");
    print("-r\t\t\trecursive mode");
    print("");
    print("macros:");
    print("@path\t\t\tdirectory of file");
    print("@fullpath\t\tpath to the file");
    return 0;
}

int print_args_error()
{
    print("unsifficient arguments, exiting (try --help)");
    return -1;
}

#define _STRING(x) #x
#define STRING(x) _STRING(x)

int main(int argc, char *argv[])
{
    QStringList args;
    for (int arg = 1; arg < argc; arg++)
        args << QString::fromLatin1(argv[arg]);

    if (!args.size()) return print_args_error();

    if (args[0] == "--help" ||
        args[0] == "-help" ||
        args[0] == "help")
        return print_usage();

    QString inc;
    if (args.contains("-i"))
    {
        if (args.indexOf("-i")+1 >= args.size()) return print_args_error();
        inc = args[args.indexOf("-i") + 1];
    }

    if (args.indexOf("-c")+1 >= args.size()) return print_args_error();
    int i = args.indexOf("-c") + 1;

    QString prgm = args[i];
    QStringList cargs;
    for (int n = i + 1; n < args.size(); n++)
        cargs << args[n];

    bool verbose = args.contains("-v");
    bool recursive = args.contains("-r");

    QString passed = "";

    for (const auto& arg : cargs)
        passed += " " + arg.trimmed();

    passed = passed.trimmed();

    QDirIterator it(
            (args[0]),
            QStringList() << inc,
            QDir::Files | QDir::NoDotAndDotDot,
            recursive ? QDirIterator::IteratorFlag::Subdirectories : QDirIterator::IteratorFlag::NoIteratorFlags);

    std::vector<QProcess*> procs;

    print(QString("using version: %1").arg(STRING(VERSION)));

    while (it.hasNext())
    {
        QStringList _cargs(cargs);
        QString _passed(passed);
        QString _prgm(prgm);
        QString path = it.next().trimmed();

        for(auto& passed : _cargs)
        {
            passed.replace("@path", QFileInfo(path).dir().absolutePath());
            passed.replace("@fullpath", path);
        }

        _passed.replace("@path", QFileInfo(path).dir().absolutePath());
        _passed.replace("@fullpath", path);

        _prgm.replace("@path", QFileInfo(path).dir().absolutePath());
        _prgm.replace("@fullpath", path);

        if(verbose) print("starting process: " + _prgm + " " + _passed);
        if(verbose) print("-------------------------------------");
        if(verbose) print("");

        QProcess* proc = new QProcess();
        if(verbose)proc->setProcessChannelMode(QProcess::ProcessChannelMode::ForwardedChannels);
        proc->start(_prgm, _cargs);
        proc->waitForFinished();

        if(verbose) print("");
        if(verbose) print("-------------------------------------");
    }

//    for (auto& proc : procs)
//        proc->waitForFinished(10000);
}
